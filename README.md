# rpi-docker-cross-compile

Dockerfile used to generate docker image based on Debian Buster that is used to cross-compile programs for Raspberry Pi or any other ARM based chip.

## Building the image

```bash
git clone https://gitlab.com/luka.kudra/rpi-docker-cross-compile.git

cd rpi-docker-cross-compile

docker build -t <tag_name> .
```

## List and/or delete images and containers

* To list running containers use the following command:

```bash
docker ps
```

* To list all images use the following command:

```bash
docker images
```

* To delete the image use the following command:

```bash
docker image rm <image_id>
```

* To the delete the image even if the container is running use the following command:

```bash
docker image rm --force <image_id>
```

## Running the image

```bash
docker run --rm -it -v $HOME/project-rpi/:/src -w /src <tag_name>
```

explanation of the previous command:

* `run` means we are running a new container.

* `--rm` indicates that we want to throw away the container when we are done. If you do this, docker saves a new container from each `run` command, which takes up space on your hard drive.

* `-it` is two options, it means "interactive" and "tty". This will let us use the command prompt inside of the container.

* `-v <host-path>:<container-path>` lets us use a directory from our host computer inside of the container.

* `-w <container-path>` is the working directory inside of the container.

* `<tag_name>` is the name of the docker image we are using

## Compiling the program

### C program

```bash
arm-linux-gnueabi-gcc -o <binary_name> <source_file>
```

### Cpp program

```bash
arm-linux-gnueabi-g++ -o <binary_name> <source_file>
```

## Link to a prebuilt image on Docker Hub

* <https://hub.docker.com/repository/docker/49197/rpi-cross-compile>
